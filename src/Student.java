public class Student {
    private String rollNumber;
    private String name;
    private int age;
    private String className;
    private float gpa;

    public Student() {
    }

    public Student(String rollNumber, String name, int age, String className, float gpa) {
        this.rollNumber = rollNumber;
        this.name = name;
        this.age = age;
        this.className = className;
        this.gpa = gpa;
    }

    public String getRollNumber() {
        return rollNumber;
    }

    public void setRollNumber(String rollNumber) {
        this.rollNumber = rollNumber;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public int getAge() {
        return age;
    }

    public void setAge(int age) {
        this.age = age;
    }

    public String getClassName() {
        return className;
    }

    public void setClassName(String className) {
        this.className = className;
    }

    public float getGpa() {
        return gpa;
    }

    public void setGpa(float gpa) {
        this.gpa = gpa;
    }
}
