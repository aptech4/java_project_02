import java.util.ArrayList;
import java.util.Collections;
import java.util.Comparator;
import java.util.Scanner;

public class Main {
    public static void main(String[] args) {
//        bai02();
//        bai03();
//        bai04();
        bai05();
    }

    static void bai01() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập số nguyên dương N: ");
        int n = scanner.nextInt();
        if (n <= 0) {
            System.out.println("Số bạn nhập không phải số nguyên dương!");
            return;
        }
        int sum = 0;
        for (int i = 1; i <= n; i++) {
            if (chiaHetCho3(i)) {
                sum += i;
            }
        }
        System.out.println("Tổng các số chia hết cho 3 là: " + sum);
    }

    static boolean chiaHetCho3(int number) {
//        if(number % 3 == 0) {
//            return true;
//        }
//        return false;
        return number % 3 == 0;
    }

    static void bai02() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào chuỗi: ");
        String text = scanner.nextLine();

        String reversed = "";
        for (int i = 0; i < text.length(); i++) {
            reversed = text.charAt(i) + reversed;
        }

//        char[] charArray = text.toCharArray();
//        for(char c : charArray) {
//            reversed = c + reversed;
//        }

        System.out.println("Chuỗi đảo ngược là: " + reversed);
    }

    static void bai03() {
        Scanner scanner = new Scanner(System.in);
        System.out.print("Nhập vào chuỗi: ");
        String text = scanner.nextLine();

        //Tìm những kí tự cấu tạo thành chuỗi vừa nhập
        String unique = "";
        for (int i = 0; i < text.length(); i++) {
            char c = text.charAt(i);
            if (unique.indexOf(c) >= 0) {
                continue;
            }
            unique += c;
        }

        //Tìm số lần xuất hiện của từng kí tự trong chuỗi
        for (int i = 0; i < unique.length(); i++) {
            int count = 0;
            for (int j = 0; j < text.length(); j++) {
                if (unique.charAt(i) == text.charAt(j)) {
                    count++;
                }
            }
            System.out.println(unique.charAt(i) + " - " + count);
        }
    }

    static void bai04() {
        ArrayList<Staff> staffList = new ArrayList<>();

        while (true) {
            System.out.println("1. Thêmmớinhânviên.\n" +
                    "2. Danhsáchnhânviên.\n" +
                    "3. InramànhìnhDanhsáchnhânviêntheothứtựtăngdầncủaTuổi.\n" +
                    "4. InramànhìnhDanhsáchnhânviênsắpxếpTêntheoAlphabet(A->Z)" +
                    "5. Thoát");

            Scanner scanner = new Scanner(System.in);

            System.out.print("Nhập lựa chọn: ");
            int option = scanner.nextInt();

            switch (option) {
                case 1:
                    scanner = new Scanner(System.in);

                    System.out.print("Nhập tên nhân viên: ");
                    String name = scanner.nextLine();

                    System.out.print("Nhập tuổi: ");
                    int age = scanner.nextInt();

                    scanner = new Scanner(System.in);
                    System.out.print("Nhập số điện thoại: ");
                    String phone = scanner.nextLine();

                    scanner = new Scanner(System.in);
                    System.out.print("Nhập chức vụ: ");
                    String role = scanner.nextLine();

                    Staff staff = new Staff(name, age, phone, role);
                    staffList.add(staff);

                    System.out.println();
                    break;
                case 2:
                    printStaffList(staffList);
                    break;
                case 3:
                    Collections.sort(staffList, new Comparator<Staff>() {
                        @Override
                        public int compare(Staff o1, Staff o2) {
                            return o1.getAge() - o2.getAge();
                        }
                    });

                    printStaffList(staffList);
                    break;
                case 4:
                    Collections.sort(staffList, new Comparator<Staff>() {
                        @Override
                        public int compare(Staff o1, Staff o2) {
                            return o1.getName().compareToIgnoreCase(o2.getName());
                        }
                    });

                    printStaffList(staffList);
                    break;
                default:
                    return;
            }

        }
    }

    static void printStaffList(ArrayList<Staff> staffList) {
        System.out.println("\nDanh sách nhân viên:");
        if (staffList.size() != 0) {
            for (int i = 0; i < staffList.size(); i++) {
                Staff st = staffList.get(i);
                System.out.printf("%s - %d - %s - %s\n", st.getName(),
                        st.getAge(), st.getPhoneNumber(), st.getRole());
            }
        } else {
            System.out.println("Chưa có nhân viên nào!");
        }
        System.out.println("\n");
    }

    static void bai05() {
        ArrayList<Student> studentList = new ArrayList<>();

        while (true) {
            System.out.println("1. Thêm mới sinh viên. \n"
                    + "2. Danh sách sinh viên.\n"
                    + "3. Sinh viên giỏi.\n"
                    + "4. Thoát.");
            System.out.print("Nhập lựa chọn: ");
            Scanner scanner = new Scanner(System.in);
            int option = scanner.nextInt();
            System.out.println(option);

            switch (option) {
                case 1:
                    Student student = getInfo();
                    studentList.add(student);
                    System.out.println("\n");
                    break;
                case 2:
                    showInfos(studentList);
                    break;
                case 3:
                    showInfosOfGoodStudent(studentList);
                    break;
                default:
                    return;
            }
        }

    }

    static Student getInfo() {
        Scanner scanner = new Scanner(System.in);

        System.out.print("Nhập mã sinh viên: ");
        String rollNumber = scanner.nextLine();

        scanner = new Scanner(System.in);
        System.out.print("Nhập tên sinh viên: ");
        String name = scanner.nextLine();

        scanner = new Scanner(System.in);
        System.out.print("Nhập tuổi: ");
        int age = scanner.nextInt();

        scanner = new Scanner(System.in);
        System.out.print("Nhập tên lớp: ");
        String className = scanner.nextLine();

        scanner = new Scanner(System.in);
        System.out.print("Nhập điểm trung bình: ");
        float gpa = scanner.nextFloat();

        Student student = new Student(rollNumber, name, age, className, gpa);
        return student;
    }

    static void showInfos(ArrayList<Student> studentList) {
        System.out.println("\nDanh sách sinh viên: ");
        for(Student student : studentList) {
            System.out.printf("%s - %s - %d - %s - %.1f\n", student.getRollNumber(),
                    student.getName(), student.getAge(), student.getClassName(), student.getGpa());
        }
        System.out.println("\n");
    }

    static void showInfosOfGoodStudent(ArrayList<Student> studentList) {
        System.out.println("\nDanh sách sinh viên giỏi: ");
        boolean hasGoodStudent = false;
        for(Student student : studentList) {
            if(student.getGpa() >= 8) {
                hasGoodStudent = true;
                System.out.printf("%s - %s - %d - %s - %.1f\n", student.getRollNumber(),
                        student.getName(), student.getAge(), student.getClassName(), student.getGpa());
            }
        }
        if(!hasGoodStudent) {
            System.out.println("Không có sinh viên giỏi nào!");
        }
        System.out.println("\n");
    }
}